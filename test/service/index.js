const { expect } = require('chai')
const { db } = require('../../src/db')
const { inputReader, parseAttempt, consumeQueues, createContest } = require('../../src/service')

module.exports = () => describe('Testing services...', () => {
  describe('Testing inputReader service...', () => {
    it('must return only one queue', () => {
      const queue = '1\n\n1 1 5 R\n1 1 10 C\n1 2 10 I\n1 2 15 R\n1 2 20 C\n\n'
      const expected = {
        total: 1,
        queues: [
          '1 1 5 R\n1 1 10 C\n1 2 10 I\n1 2 15 R\n1 2 20 C',
        ]
      }

      expect(inputReader(queue)).to.deep.equal(expected)
    })

    it('must return two queues', () => {
      const queue = '2\n\n1 1 5 R\n1 1 10 C\n1 2 10 I\n1 2 15 R\n1 2 20 C\n\n2 1 5 R\n2 1 10 C\n2 2 10 I\n2 2 15 R\n2 2 15 C\n\n'
      const expected = {
        total: 2,
        queues: [
          '1 1 5 R\n1 1 10 C\n1 2 10 I\n1 2 15 R\n1 2 20 C',
          '2 1 5 R\n2 1 10 C\n2 2 10 I\n2 2 15 R\n2 2 15 C'
        ]
      }

      expect(inputReader(queue)).to.deep.equal(expected)
    })
  })

  describe('Testing parseAttempt service...', () => {
    it('must return a valid attempt', () => {
      const attempt = '1 2 3 R'
      const expected = {
        id: 1,
        problem: 2,
        time: 3,
        status: 'R'
      }

      expect(parseAttempt(attempt)).to.deep.equal(expected)
    })
  })

  describe('Testing consumeQueues service...', () => {
    it('must return an array of valid attempts', () => {
      const queues = [
        '1 1 5 R\n1 1 10 C\n1 2 10 I\n1 2 15 R\n1 2 20 C',
        '2 1 5 R\n2 1 10 C\n2 2 10 I\n2 2 15 R\n2 2 15 C'
      ]

      const expected = [
        {
          id: 2,
          problem: 1,
          time: 5,
          status: 'R'
        },
        {
          id: 2,
          problem: 1,
          time: 10,
          status: 'C'
        },
        {
          id: 2,
          problem: 2,
          time: 10,
          status: 'I'
        },
        {
          id: 2,
          problem: 2,
          time: 15,
          status: 'R'
        },
        {
          id: 2,
          problem: 2,
          time: 15,
          status: 'C'
        },
        {
          id: 1,
          problem: 1,
          time: 5,
          status: 'R'
        },
        {
          id: 1,
          problem: 1,
          time: 10,
          status: 'C'
        },
        {
          id: 1,
          problem: 2,
          time: 10,
          status: 'I'
        },
        {
          id: 1,
          problem: 2,
          time: 15,
          status: 'R'
        },
        {
          id: 1,
          problem: 2,
          time: 20,
          status: 'C'
        },
      ]

      expect(consumeQueues(queues)).to.deep.equal(expected)
    })
  })

  describe('Testing createContest service...', () => {
    beforeEach(() => {
      db.removeCollection('contest')
      db.addCollection('contest')
    })

    it('must return a valid contest', () => {
      const total = 2
      const attempts = [
        {
          id: 2,
          problem: 1,
          time: 5,
          status: 'R'
        },
        {
          id: 2,
          problem: 1,
          time: 10,
          status: 'C'
        },
        {
          id: 2,
          problem: 2,
          time: 10,
          status: 'I'
        },
        {
          id: 2,
          problem: 2,
          time: 15,
          status: 'R'
        },
        {
          id: 2,
          problem: 2,
          time: 15,
          status: 'C'
        },
        {
          id: 1,
          problem: 1,
          time: 5,
          status: 'R'
        },
        {
          id: 1,
          problem: 1,
          time: 10,
          status: 'C'
        },
        {
          id: 1,
          problem: 2,
          time: 10,
          status: 'I'
        },
        {
          id: 1,
          problem: 2,
          time: 15,
          status: 'R'
        },
        {
          id: 1,
          problem: 2,
          time: 20,
          status: 'C'
        },
      ]

      const expectedContestants = [
        {
          id: 2,
          participating: true,
          count: 2,
          penalty: 55,
          solved: [
            1, 
            2
          ]
        },
        {
          id: 1,
          participating: true,
          count: 2,
          penalty: 60,
          solved: [
            1, 
            2
          ]
        }
      ]

      expect(createContest(attempts, total).total).to.equal(total)
      expect(createContest(attempts, total).contestants()).to.deep.equal(expectedContestants)
    })

    it('must return a valid scoreboard', () => {
      const total = 2
      const attempts = [
        {
          id: 3,
          problem: 3,
          time: 5,
          status: 'R'
        },
        {
          id: 3,
          problem: 3,
          time: 10,
          status: 'C'
        },
        {
          id: 3,
          problem: 4,
          time: 10,
          status: 'I'
        },
        {
          id: 3,
          problem: 4,
          time: 15,
          status: 'R'
        },
        {
          id: 3,
          problem: 4,
          time: 15,
          status: 'C'
        },
        {
          id: 4,
          problem: 3,
          time: 5,
          status: 'R'
        },
        {
          id: 4,
          problem: 3,
          time: 10,
          status: 'C'
        },
        {
          id: 4,
          problem: 4,
          time: 10,
          status: 'I'
        },
        {
          id: 4,
          problem: 4,
          time: 15,
          status: 'R'
        },
        {
          id: 4,
          problem: 4,
          time: 20,
          status: 'I'
        },
      ]

      const expected = '3 2 55\n4 1 80'

      expect(createContest(attempts, total).scoreboard()).to.equal(expected)
    })
  })
})
