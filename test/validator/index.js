const { expect } = require('chai')

const { validateInput, validateLine } = require('../../src/validator')

module.exports = () => describe('Testing validators...', () => {
  describe('Testing validateInput validator...', () => {
    it('must not throw any error on a well formed queue', () => {
      const queue = '3\n\n1 1 3 R\n2 1 6 U\n1 1 9 I\n1 2 12 I\n2 1 9 I\n\n1 1 20 R\n1 2 17 I\n2 3 5 U\n2 3 8 R\n1 2 15 C\n2 3 12 I\n2 3 18 C\n2 2 31 C\n1 1 42 C\n2 2 37 C\n\n'

      expect(() => validateInput(queue)).to.not.throw()
    })

    it('must throw an error if first total is not an integer', () => {
      const queue = 'A\n\n3 3 3 C\n\n'

      expect(() => validateInput(queue)).to.throw('Error: first argument must be the number of board entries.')
    })

    it('must throw an error if no blank line after total', () => {
      const queue = '1\n3 3 3 C\n\n'

      expect(() => validateInput(queue)).to.throw('Error: only one blank line is allowed before score queues.')
    })

    it('must throw an error if input not properly ended', () => {
      const queue = '1\n\n3 3 3 C\n'

      expect(() => validateInput(queue)).to.throw('Error: input has not ended properly (two blank lines).')
    })

    it('must throw an error if queue entry length is not 4', () => {
      const queue1 = '1\n\n3 3 C\n\n'
      const queue2 = '1\n\n3 3 3 3 C\n\n'

      expect(() => validateInput(queue1)).to.throw('Error: a valid entry has 4 parameters: id, problem, time, status.')
      expect(() => validateInput(queue2)).to.throw('Error: a valid entry has 4 parameters: id, problem, time, status.')
    })
  })

  describe('Testing validateLine validator...', () => {
    it('must not throw any error on a well formed line', () => {
      const line = '1 1 3 R'

      expect(() => validateLine(line)).to.not.throw()
    })
    
    it('must throw an error if any of the firt 3 parameters are not an integer', () => {
      const line = 'A 1 3 R'

      expect(() => validateLine(line)).to.throw('Error: id, problem and time must be integers.')
    })    
    
    it('must throw an error if any of the last parameter is not one of C, I, R, U, E', () => {
      const line = '1 1 3 S'

      expect(() => validateLine(line)).to.throw('Error: status must be one of C, I, R, U, E.')
    })    
  })
})