const validatorSuite = require('./validator')
const serviceSuite = require('./service')

describe('Testing core functions...', () => {
  validatorSuite()
  serviceSuite()
})