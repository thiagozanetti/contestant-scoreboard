const { validateInput, validateLine } = require('../validator')
const Attempt = require('../model/attempt')
const Queue = require('../model/queue')
const Contest = require('../model/contest')

const inputReader = input => {
  validateInput(input)

  return Queue(input)
}

const parseAttempt = entry => {
  validateLine(entry)

  return Attempt(entry)
}

const consumeQueues = queues => {
  if (!Array.isArray(queues)) {
    queues = [queues]
  }

  return queues.
    map(queue => queue.split('\n').
      map(attempt => parseAttempt(attempt))).
    reduce((it, acc) => acc.concat(it), [])

}

const createContest = (attempts, total)  => {
  const contest = Contest(total)

  attempts.forEach(attempt => contest.judgeAttempt(attempt))

  return contest
}

const retrieveContest = () => {
  const contest = Contest()

  return contest
}

module.exports = {
  inputReader,
  parseAttempt,
  consumeQueues,
  createContest,
  retrieveContest
}