const { GITHUB_TOKEN } = require('../../constants')
const { post, get } = require('axios')

const retrieveGist = async (id=null) => {
  return await get(
    `https://api.github.com/gists${id ? '/'.concat(id) : ''}`,
    {
      method: 'GET',
      headers: {
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': `Bearer ${GITHUB_TOKEN}`
      }
    }
  ).then(response => response.data)
}

const createGist = async ({ description, files, public=false }) => {
  return await post(
    'https://api.github.com/gists',
    {
      description,
      files,
      public
    }, 
    {
      method: 'POST',
      headers: {
        'Accept': 'application/vnd.github.v3+json',
        'Authorization': `Bearer ${GITHUB_TOKEN}`
      }
    }
  ).then(response => response.data).catch(error => error)
}

module.exports = {
  retrieveGist,
  createGist
}