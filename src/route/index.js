const { inputReader, consumeQueues, createContest, retrieveContest } = require('../service')
const { createGist, retrieveGist } = require('../service/github')

module.exports.applyRouting = app => {
  app.get('/', (req, res) => res.send('Hello World!'))

  app.post('/queue', (req, res) => {
    const input = inputReader(req.body)

    const attempts = consumeQueues(input.queues)

    const contest = createContest(attempts, input.total)

    res.send(contest.scoreboard())
  })

  app.get('/scoreboard', (req, res) => {
    const contest = retrieveContest()

    res.send(contest.scoreboard())
  })

  app.post('/gists', async (req, res) => {
    const gist = req.body
    
    res.send(await createGist(gist))
  })
  
  app.get('/gists/:id?', async (req, res) => {
    const id = req.params.id

    res.send(await retrieveGist(id))
  })
}

