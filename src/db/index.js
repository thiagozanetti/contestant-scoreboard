const loki = require('lokijs')

const db = new loki('challenge.json', {
  autoload: true,
  autosave: true,
  autosaveInterval: 5000
})

const dbCollection = name => {
  return db.getCollection(name) || db.addCollection(name)
}

module.exports = {
  db,
  dbCollection
}