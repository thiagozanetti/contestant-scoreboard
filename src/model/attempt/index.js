const Attempt = entry => {
  const [id, problem, time, status] = entry.split(' ')
    .map(part => Number.isInteger(Number(part)) && Number(part) || part)

  return {
    id, 
    problem, 
    time, 
    status
  }
}

module.exports = Attempt