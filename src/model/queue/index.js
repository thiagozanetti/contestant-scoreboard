const Queue = input => {
  const total = Number(input[0])
  const queues = input.slice(3).slice(0, -2).split('\n\n')

  return {
    total,
    queues
  }
}

module.exports = Queue