const Contestant = (id, participating=false, count=0, penalty=0) => ({id, participating, count, penalty, solved: []})

module.exports = Contestant
