const Contestant = require('../contestant')
const { sortCriteria } = require('../../criteria')
const { dbCollection } = require('../../db')

const Contest = (total=null) => {
  const collection = dbCollection('contest')

  let contest = collection.findOne()

  if (!contest) {
    contest = collection.insert({ total, contestants: [] })
  }

  const contestants = () => contest.contestants
  const findContestant = id => contest.contestants.find(contestant => contestant.id === id)
  const participating = id => !!findContestant(id)
  const solved = (contestant, problem) => !!contestant.solved.find(entry => entry === problem)
  const addContestant = id => !participating(id) && contest.contestants.push(Contestant(id, true))
  const updateCollection = () => collection.update(contest)

  const computeComplete = ({ id, problem, time }) => {
    const contestant = findContestant(id)

    if (contestant && !solved(contestant, problem)) {
      contestant.solved.push(problem)
      contestant.count += 1
      contestant.penalty += time
    }
  }

  const computeIncomplete = ({ id, problem, time }) => {
    const contestant = findContestant(id)

    if (contestant && !solved(contestant, problem)) {
      contestant.penalty += (time + 20)
    }
  }

  const judgeAttempt = attempt => {
    addContestant(attempt.id)

    switch (attempt.status) {
    case 'C':
      computeComplete(attempt)
      updateCollection()
      break
    
    case 'I':
      computeIncomplete(attempt)
      updateCollection()
      break
    
    default:
      break
    }
  }

  const scoreboard = () => {
    return contest.contestants.
      filter(c => c.participating).
      sort(sortCriteria).
      map(c => `${c.id} ${c.count} ${c.penalty}`).join('\n') || ''
  }

  return {
    total,
    contestants,
    judgeAttempt,
    scoreboard
  }
}

module.exports = Contest
