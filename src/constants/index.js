require('dotenv').config()

const SERVER_HOST = process.env.SERVER_HOST || '0.0.0.0'
const SERVER_PORT = process.env.SERVER_PORT || 8001
const GITHUB_TOKEN = process.env.GITHUB_TOKEN || null

module.exports = {
  SERVER_HOST,
  SERVER_PORT,
  GITHUB_TOKEN
}