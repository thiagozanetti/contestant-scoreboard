const sortCriteria = (a, b) => {
  if (a.solved < b.solved) {
    return 1
  } else if (a.solved > b.solved) {
    return -1
  }

  if (a.penalty > b.penalty) {
    return 1
  } else if (a.penalty < b.penalty) {
    return -1
  }

  return a.id > b.id ? 1 : a.id < b.id ? -1 : 0
}

module.exports = {
  sortCriteria
}