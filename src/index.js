const app = require('express')()
const bodyParser = require('body-parser')

const router = require('./route')
const { SERVER_HOST, SERVER_PORT } = require('./constants')

app.use(bodyParser.json({ strict: false }))
app.use(bodyParser.urlencoded({ extended: true }))

router.applyRouting(app)

app.listen(SERVER_PORT, SERVER_HOST, () => {
  console.log(`Listening on ${SERVER_HOST}:${SERVER_PORT}`)
})
