const validateInput = input => {
  const lines = input.split('\n')

  const total = Number(lines[0])

  if (!Number.isInteger(total)) {
    throw new Error('Error: first argument must be the number of board entries.')
  }

  if (lines[1] !== '' || (lines[1] === '' && lines[2] === '')) {
    throw new Error('Error: only one blank line is allowed before score queues.')
  }

  if (!(lines[lines.length -1] === '' && lines[lines.length -2] === '')) {
    throw new Error('Error: input has not ended properly (two blank lines).')
  }

  const queues = input.slice(3).slice(0, -2).split('\n\n')

  if (!queues.every(queue => queue.split('\n').every(entry => entry.split(' ').length === 4))) {
    throw new Error('Error: a valid entry has 4 parameters: id, problem, time, status.')
  }
}

const validateLine = entry => {
  const [id=null, problem=null, time=null, status=null] = entry.split(' ')

  if (!Number.isInteger(Number(id)) || !Number.isInteger(Number(problem)) || !Number.isInteger(Number(time))) {
    throw new Error('Error: id, problem and time must be integers.')
  }

  if (!status || status.length !== 1 || !['C', 'I', 'R', 'U', 'E'].some(l => l === status)) {
    throw new Error('Error: status must be one of C, I, R, U, E.')
  }
}

const validateAttempt = (attempt, total) => {
  if (attempt.problem > total) {
    throw new Error('Error: problem number is greater than the total number of problems to solve.')
  }
}

module.exports = {
  validateInput,
  validateLine,
  validateAttempt
}