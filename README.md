# contestant-scoreboard
Scoreboard problem solved in JS

## How to run

We are using **dotenv** to set application params, such as `SERVER_HOST`, `SERVER_PORT` and `GITHUB_TOKEN`, from environment variables. You can create a copy of the `.env-example` file and edit with proper values:

```sh
$ cp .env-example .env
$ $EDITOR .env #or use your prefered editor.
```

To install the dependencies:

```sh
$ npm install
```

To run the test suite:

```sh
$ npm test
```

To start the application:

```sh
$ npm start
```

NOTE: To work with the gists endpoint you have to generate a [Github Personal Token](https://github.com/settings/tokens) with the `gist` scope and set in your `.env` file.